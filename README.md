# README

A simple API built in Ruby on Rails that returns example sentences scraped from Google Books using a given word and specified language. Entry-points exist additionally for processing votes on the usefulness of a sentence as well as for the submission of alternative sentences.
